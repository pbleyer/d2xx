using System;
using System.Runtime.InteropServices;

public class D2XX 
{
/* Device status */
	public const int
		OK = 0,
		INVALID_HANDLE = 1,
		DEVICE_NOT_FOUND = 2,
		DEVICE_NOT_OPENED = 3,
		IO_ERROR = 4,
		INSUFFICIENT_RESOURCES = 5,
		INVALID_PARAMETER = 6,
		INVALID_BAUD_RATE = 7,
		DEVICE_NOT_OPENED_FOR_ERASE = 8,
		DEVICE_NOT_OPENED_FOR_WRITE = 9,
		FAILED_TO_WRITE_DEVICE = 10,
		EEPROM_READ_FAILED = 11,
		EEPROM_WRITE_FAILED = 12,
		EEPROM_ERASE_FAILED = 13,
		EEPROM_NOT_PRESENT = 14,
		EEPROM_NOT_PROGRAMMED = 15,
		INVALID_ARGS = 16,
		NOT_SUPPORTED = 17,
		OTHER_ERROR = 18,
		DEVICE_LIST_NOT_READY = 19;

	private static string[] status_message =
		{
			"Ok",
			"Invalid Handle",
			"Device Not Found",
			"Device Not Opened",
			"Io Error",
			"Insufficient Resources",
			"Invalid Parameter",
			"Invalid Baud Rate",
			"Device Not Opened For Erase",
			"Device Not Opened For Write",
			"Failed To Write Device",
			"Eeprom Read Failed",
			"Eeprom Write Failed",
			"Eeprom Erase Failed",
			"Eeprom Not Present",
			"Eeprom Not Programmed",
			"Invalid Args",
			"Not Supported",
			"Other Error",
			"Device List Not Ready"
		};

	/* openEx flags */
	public const int
		OPEN_BY_SERIAL_NUMBER = 1<<0,
		OPEN_BY_DESCRIPTION = 1<<1,
		OPEN_BY_LOCATION = 1<<2;

	/* listDevices flags (used in conjunction with openEx flags) */
	public const int
		LIST_NUMBER_ONLY = 1<<31,
		LIST_BY_INDEX = 1<<30,
		LIST_ALL = 1<<29,
		LIST_MASK = LIST_NUMBER_ONLY | LIST_BY_INDEX | LIST_ALL;

	/* Baud rates */
	public const int
		BAUD_300 = 300,
		BAUD_600 = 600,
		BAUD_1200 = 1200,
		BAUD_2400 = 2400,
		BAUD_4800 = 4800,
		BAUD_9600 = 9600,
		BAUD_14400 = 14400,
		BAUD_19200 = 19200,
		BAUD_38400 = 38400,
		BAUD_57600 = 57600,
		BAUD_115200 = 115200,
		BAUD_230400 = 230400,
		BAUD_460800 = 460800,
		BAUD_921600 = 921600;

	/* Word lengths */
	public const int
		BITS_8 = 8,
		BITS_7 = 7,
		BITS_6 = 6,
		BITS_5 = 5;

	/* Stop bits */
	public const int
		STOP_BITS_1 = 0,
		STOP_BITS_1_5 = 1,
		STOP_BITS_2 = 2;

	/* Parity */
	public const int
		PARITY_NONE = 0,
		PARITY_ODD = 1,
		PARITY_EVEN = 2,
		PARITY_MARK = 3,
		PARITY_SPACE = 4;

	/* Flow control */
	public const int
		FLOW_NONE = 0,
		FLOW_RTS_CTS = 1<<8,
		FLOW_DTR_DSR = 1<<9,
		FLOW_XON_XOFF = 1<<10;

	/* Purge rx and tx buffers */
	public const int
		PURGE_RX = 1<<0,
		PURGE_TX = 1<<1;

	/* Events */
	public const int
		EVENT_RXCHAR = 1<<0,
		EVENT_MODEM_STATUS = 1<<1;

	/* Timeouts */
	public const int
		DEFAULT_RX_TIMEOUT = 300,
		DEFAULT_TX_TIMEOUT = 300;

	/* Device types */
	public const int
		DEVICE_BM = 0,
		DEVICE_AM = 1,
		DEVICE_100AX = 2,
		DEVICE_UNKNOWN = 3,
		DEVICE_2232C = 4,
		DEVICE_232R = 5;

	public IntPtr Handle = IntPtr.Zero;

	[DllImport("ftd2xx.dll")] //EntryPoint = "FT_Open"
	public static extern int FT_Open(int device, out IntPtr handle);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_OpenEx(IntPtr arg, int flags, out IntPtr handle);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_Close(IntPtr handle);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_ListDevices(IntPtr arg1, IntPtr arg2, int flags);

	[DllImport("ftd2xx.dll")] // CharSet = CharSet.Ansi
	public static extern int FT_Read(IntPtr handle, IntPtr buf, int size, out int ret); 

	[DllImport("ftd2xx.dll")]
	public static extern int FT_Write(IntPtr handle, IntPtr buf, int size, out int ret);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_SetBaudRate(IntPtr handle, uint br);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_SetDivisor(IntPtr handle, ushort div);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_SetDataCharacteristics(IntPtr handle, byte wl, byte sb, byte par);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_SetFlowControl(IntPtr handle, ushort fc, byte xon, byte xoff);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_ResetDevice(IntPtr handle);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_GetModemStatus(IntPtr handle, out uint ms);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_SetChars(IntPtr handle, byte evc, byte eve, byte erc, byte ere);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_Purge(IntPtr handle, uint ms);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_SetTimeouts(IntPtr handle, uint rt, uint wt);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_GetQueueStatus(IntPtr handle, out int bs);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_GetStatus(IntPtr handle, out int rb, out int wb, out int ev);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_SetBreakOn(IntPtr handle);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_SetBreakOff(IntPtr handle);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_GetDeviceInfo(IntPtr handle, out uint dev, out int id, byte[] sn, byte[] dc, IntPtr d);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_SetDtr(IntPtr handle);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_ClrDtr(IntPtr handle);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_SetRts(IntPtr handle);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_ClrRts(IntPtr handle);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_SetWaitMask(IntPtr handle, int msk);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_WaitOnMask(IntPtr handle, out int msk);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_GetEventStatus(IntPtr handle, out int es);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_SetEventNotification(IntPtr handle, int msk, IntPtr par);
	
	[DllImport("ftd2xx.dll")]
	public static extern int FT_SetLatencyTimer(IntPtr handle, byte lt);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_GetLatencyTimer(IntPtr handle, out byte lt);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_SetBitMode(IntPtr handle, byte msk, byte ena);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_GetBitMode(IntPtr handle, out byte bm);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_SetUSBParameters(IntPtr handle, uint its, uint ots);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_SetDeadmanTimeout(IntPtr handle, uint dt);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_StopInTask(IntPtr handle);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_RestartInTask(IntPtr handle);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_SetResetPipeRetryCount(IntPtr handle, int cnt);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_ResetPort(IntPtr handle);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_CyclePort(IntPtr handle);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_GetDriverVersion(IntPtr handle, out int ver);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_GetLibraryVersion(out int ver);

	[DllImport("ftd2xx.dll")]
	public static extern int FT_Rescan();

	[DllImport("ftd2xx.dll")]
	public static extern int FT_Reload(int vid, int pid);

	//[DllImport("ftd2xx.dll")] // CharSet = CharSet.Ansi
	//public static extern int FT_GetDeviceInfo(IntPtr handle, 
	//  FT_DEVICE* lpftDevice,
	//  out int dev,
	//  byte[] ser,
	//  byte[] desc,
	//  IntPtr dum
	//    );

	public D2XX()
	{
	}

	public D2XX(int device)
	{
		int st = FT_Open(device, out Handle);
		if (st != OK) throw new Exception(status_message[st]);
	}

	public D2XX(string s, int f)
	{
		byte[] bs = (new System.Text.ASCIIEncoding()).GetBytes(s);
		int st;
		unsafe
		{
			fixed (byte* ba = bs)
				st = FT_OpenEx((IntPtr)ba, f, out Handle);
		}
		if (st != OK) throw new Exception(status_message[st]);
	}

	~D2XX()
	{
		Close();
	}

	public void Open(int device)
	{
		if (IsOpen()) throw new Exception("Device Already Opened");
		int st = FT_Open(device, out Handle);
		if (st != OK) throw new Exception(status_message[st]);
	}

	public void OpenEx(int location)
	{
		if (IsOpen()) throw new Exception("Device Already Opened");
		int st = FT_OpenEx((IntPtr)location, OPEN_BY_LOCATION, out Handle);
		if (st != OK) throw new Exception(status_message[st]);
	}

	public void OpenEx(string s, int flags)
	{
		if (IsOpen()) throw new Exception("Device Already Opened");
		byte[] bs = (new System.Text.ASCIIEncoding()).GetBytes(s);
		int st;
		unsafe
		{
			fixed (byte* ba = bs)
				st = FT_OpenEx((IntPtr)ba, flags, out Handle);
		}
		if (st != OK) throw new Exception(status_message[st]);
	}

	public bool IsOpen()
	{
		return Handle != IntPtr.Zero;
	}

	public void Close()
	{
		if (Handle.Equals(IntPtr.Zero)) return;
		int st = FT_Close(Handle);
		if (st != OK) throw new Exception(status_message[st]);
		Handle = IntPtr.Zero;
	}

	public static object ListDevices(int flags)
	{
		int st, n;
		// first search for number of devices
		unsafe
		{
			st = FT_ListDevices((IntPtr)(&n), IntPtr.Zero, LIST_NUMBER_ONLY);
		}
		if (st != OK) throw new Exception(status_message[st]);
		if (n == 0) return null; // no devices

		// n = (n <= MAX_DEVICES) ? n : MAX_DEVICES;

		if ((flags & OPEN_BY_LOCATION) != 0) {
			int[] result = new int[n];

			unsafe
			{
				fixed (int* ba = result)
				{
					st = FT_ListDevices((IntPtr)ba, (IntPtr)(&n), LIST_ALL | flags);
				}
			}
			if (st != OK) throw new Exception(status_message[st]);
			return result;
		}
		else {
			int desc = 256;
			IntPtr buf = Marshal.AllocHGlobal(n * desc + 1);
			IntPtr[] bp = new IntPtr[n + 1]; // n devices plus null terminator
			string[] result = new string[n];

			unsafe {
				sbyte* sbp = (sbyte*)buf.ToPointer();
				for (int i = 0; i < n; ++i) bp[i] = (IntPtr)(sbp + i * desc);
				bp[n] = IntPtr.Zero;
				fixed (void* p = bp)
				{
					st = FT_ListDevices((IntPtr)p, (IntPtr)(&n), LIST_ALL | flags);
				}
				if (st != OK)
				{
					Marshal.FreeHGlobal(buf);
					throw new Exception(status_message[st]);
				}
				for (int i = 0; i < n; ++i) result[i] = new String(sbp + i * desc);
				Marshal.FreeHGlobal(buf);
			}
			return result;
		}

		// return null;
	}

	public static int[] ListDevicesByLocation()
	{
		return (int[])ListDevices(OPEN_BY_LOCATION);
	}

	public static string[] ListDevicesBySerialNumber()
	{
		return (string[])ListDevices(OPEN_BY_SERIAL_NUMBER);
	}

	public static string[] ListDevicesByDescription()
	{
		return (string[])ListDevices(OPEN_BY_DESCRIPTION);
	}
	
	public int Read(byte[] buf, int off, int len)
	{
		if (buf == null) 
			throw new NullReferenceException();
		else if ((off < 0) || (off > buf.Length) || (len < 0)
			|| ((off + len) > buf.Length) || ((off + len) < 0)) 
			throw new IndexOutOfRangeException();
		else if (len == 0)
			return 0;

		int ret, st;
		unsafe
		{
			fixed (byte* bp = buf)
				st = FT_Read(Handle, (IntPtr)(bp + off), len, out ret);
		}

		//unsafe
		//{
		//  fixed (byte* ba = &buf[off])
		//  {
		//    st = FT_Read(Handle, ba, len, out ret);
		//  }
		//}

		//IntPtr mb = Marshal.AllocHGlobal(len);
		//unsafe
		//{
		//  sbyte* sb = (sbyte*)mb.ToPointer();
		//  st = FT_Read(Handle, (IntPtr)sb, len, out ret);
		//}
		//Marshal.Copy(mb, buf, 0, len);
		//Marshal.FreeHGlobal(mb);
	
		if (st != OK) throw new Exception(status_message[st]);
		return ret;
	}

	public int Write(byte[] buf, int off, int len)
	{
		if (buf == null)
			throw new NullReferenceException();
		else if ((off < 0) || (off > buf.Length) || (len < 0)
			|| ((off + len) > buf.Length) || ((off + len) < 0))
			throw new IndexOutOfRangeException();
		else if (len == 0)
			return 0;

		int ret, st;
		unsafe
		{
			fixed (byte* bp = &buf[off])
				st = FT_Write(Handle, (IntPtr)bp, len, out ret);
		}

		if (st != OK) throw new Exception(status_message[st]);
		return ret;
	}

	/** Read bytes from device helper function */
	public byte[] Read(int s)
	{
		byte[] b = new byte[s];
		int r = Read(b);
		if (r == b.Length) return b;
		else {
			byte[] c = new byte[r];
			Array.Copy(b, 0, c, 0, r);
			return c;
		}
	}

	/** Read bytes from device helper function */
	public int Read(byte[] b) {
		return Read(b, 0, b.Length);
	}

	/** Force Read single byte from device */
	public int Read() {
		byte[] b = new byte[1];
		if (Read(b) != 1) throw new Exception(status_message[IO_ERROR]);
		return b[0] & 0xff;
	}

	/** Write single byte to device */
	public int Write(int b) {
		byte[] c = new byte[1];
		c[0] = (byte)b;
		return Write(c);
	}

	/** Write bytes to device helper function */
	public int Write(byte[] b) {
		return Write(b, 0, b.Length);
	}

	/** Read bytes as string to device helper function */
	public string Get(int l)
	{
		System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
		byte[] b = Read(l);
		return enc.GetString(b);
	}

	/** Write string bytes to device helper function */
	public int Put(string s)
	{
		System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
		byte[] b = enc.GetBytes(s);
		return Write(b, 0, b.Length);
	}

	public void SetBaudRate(uint br)
	{
		int st = FT_SetBaudRate(Handle, br);
		if (st != OK) throw new Exception(status_message[st]);
	}

	public void SetDivisor(ushort div)
	{
		int st = FT_SetDivisor(Handle, div);
		if (st != OK) throw new Exception(status_message[st]);
	}

	public void SetDataCharacteristics(byte wl, byte sb, byte par)
	{
		int st = FT_SetDataCharacteristics(Handle, wl, sb, par);
		if (st != OK) throw new Exception(status_message[st]);
	}

	public void SetFlowControl(ushort fc, byte xon, byte xoff)
	{
		int st = FT_SetFlowControl(Handle, fc, xon, xoff);
		if (st != OK) throw new Exception(status_message[st]);
	}

	public void ResetDevice()
	{
		int st = FT_ResetDevice(Handle);
		if (st != OK) throw new Exception(status_message[st]);
	}

	public bool Dtr 
	{
		set
		{
			int st = (value) ? FT_SetDtr(Handle) : FT_ClrDtr(Handle);
			if (st != OK) throw new Exception(status_message[st]);
		}
		// get { return false; }
	}

	public bool Rts
	{
		set
		{
			int st = (value) ? FT_SetRts(Handle) : FT_ClrRts(Handle);
			if (st != OK) throw new Exception(status_message[st]);
		}
		// get { return false; }
	}

	public uint GetModemStatus()
	{
		uint ms;
		int st = FT_GetModemStatus(Handle, out ms);
		if (st != OK) throw new Exception(status_message[st]);
		return ms;
	}

	public void SetChars(byte evc, byte eve, byte erc, byte ere)
	{
		int st = FT_SetChars(Handle, evc, eve, erc, ere);
		if (st != OK) throw new Exception(status_message[st]);
	}

	public void Purge(uint msk)
	{
		int st = FT_Purge(Handle, msk);
		if (st != OK) throw new Exception(status_message[st]);
	}

	public void SetTimeouts(uint rt, uint wt)
	{
		int st = FT_SetTimeouts(Handle, rt, wt);
		if (st != OK) throw new Exception(status_message[st]);
	}

	public int GetQueueStatus()
	{
		int rb, st = FT_GetQueueStatus(Handle, out rb);
		if (st != OK) throw new Exception(status_message[st]);
		return rb;
	}

	public void GetStatus(out int rt, out int wb, out int ev)
	{
		int st = FT_GetStatus(Handle, out rt, out wb, out ev);
		if (st != OK) throw new Exception(status_message[st]);
	}

	public bool Break
	{
		set
		{
			int st = (value) ? FT_SetBreakOn(Handle) : FT_SetBreakOff(Handle);
			if (st != OK) throw new Exception(status_message[st]);
		}
		// get { return false; }
	}

	public int GetEventStatus()
	{
		int es, st = FT_GetEventStatus(Handle, out es);
		if (st != OK) throw new Exception(status_message[st]);
		return es;
	}

	//public int EventStatus
	//{
	//  get
	//  {
	//    int es, st = FT_GetEventStatus(Handle, out es);
	//    if (st != OK) throw new Exception(status_message[st]);
	//    return es;
	//  }
	//}

	public void SetWaitMask(int msk)
	{
		int st = FT_SetWaitMask(Handle, msk);
		if (st != OK) throw new Exception(status_message[st]);
	}

	public int WaitOnMask()
	{
		int msk, st = FT_WaitOnMask(Handle, out msk);
		if (st != OK) throw new Exception(status_message[st]);
		return msk;
	}

	//public int WaitMask
	//{
	//  set
	//  {
	//    int st = FT_SetWaitMask(Handle, value);
	//    if (st != OK) throw new Exception(status_message[st]);
	//  }
	//  get
	//  {
	//    int wm, st = FT_WaitOnMask(Handle, out wm);
	//    if (st != OK) throw new Exception(status_message[st]);
	//    return wm;
	//  }
	//}

	public void SetEventNotification(int msk, IntPtr par)
	{
		int st = FT_SetEventNotification(Handle, msk, par);
		if (st != OK) throw new Exception(status_message[st]);
	}

	public void SetLatencyTimer(byte lt)
	{
		int st = FT_SetLatencyTimer(Handle, lt);
		if (st != OK) throw new Exception(status_message[st]);
	}

	public byte GetLatencyTimer()
	{
		byte lt;
		int st = FT_GetLatencyTimer(Handle, out lt);
		if (st != OK) throw new Exception(status_message[st]);
		return lt;
	}

	//public byte LatencyTimer
	//{
	//  set
	//  {
	//    int st = FT_SetLatencyTimer(Handle, value);
	//    if (st != OK) throw new Exception(status_message[st]);
	//  }
	//  get
	//  {
	//    byte lt;
	//    int st = FT_GetLatencyTimer(Handle, out lt);
	//    if (st != OK) throw new Exception(status_message[st]);
	//    return lt;
	//  }
	//}

	public void SetBitMode(byte msk, byte ena)
	{
		int st = FT_SetBitMode(Handle, msk, ena);
		if (st != OK) throw new Exception(status_message[st]);
	}

	public byte GetBitMode()
	{
		byte bm;
		int st = FT_GetBitMode(Handle, out bm);
		if (st != OK) throw new Exception(status_message[st]);
		return bm;
	}

	public void SetUSBParameters(uint its, uint ots)
	{
		int st = FT_SetUSBParameters(Handle, its, ots);
		if (st != OK) throw new Exception(status_message[st]);
	}

	public void SetDeadmanTimeout(uint dt)
	{
		int st = FT_SetDeadmanTimeout(Handle, dt);
		if (st != OK) throw new Exception(status_message[st]);
	}

	public void StopInTask()
	{
		int st = FT_StopInTask(Handle);
		if (st != OK) throw new Exception(status_message[st]);
	}

	public void RestartInTask()
	{
		int st = FT_RestartInTask(Handle);
		if (st != OK) throw new Exception(status_message[st]);
	}

	public void SetResetPipeRetryCount(int cnt)
	{
		int st = FT_SetResetPipeRetryCount(Handle, cnt);
		if (st != OK) throw new Exception(status_message[st]);
	}

	public void ResetPort()
	{
		int st = FT_ResetPort(Handle);
		if (st != OK) throw new Exception(status_message[st]);
	}

	public void CyclePort()
	{
		int st = FT_CyclePort(Handle);
		if (st != OK) throw new Exception(status_message[st]);
	}

	public int GetDriverVersion()
	{
		int ver;
		int st = FT_GetDriverVersion(Handle, out ver);
		if (st != OK) throw new Exception(status_message[st]);
		return ver;
	}

	public static int GetLibraryVersion()
	{
		int ver;
		int st = FT_GetLibraryVersion(out ver);
		if (st != OK) throw new Exception(status_message[st]);
		return ver;
	}

	public static void Rescan()
	{
		int st = FT_Rescan();
		if (st != OK) throw new Exception(status_message[st]);
	}

	public static void Reload(int vid, int pid)
	{
		int st = FT_Reload(vid, pid);
		if (st != OK) throw new Exception(status_message[st]);
	}

	// Event interface
	public class D2XXEventArgs: EventArgs
	{
		public int Status, Received, Transmitted;
		public D2XXEventArgs(int r, int t, int s)
		{
			Received = r; Transmitted = t; Status = s;
		}
	}

	public delegate void D2XXEventHandler(object sender, D2XXEventArgs args);

	public class D2XXEvent 
	{
		[DllImport("kernel32.dll")]
		static extern IntPtr CreateEvent(IntPtr lpEventAttributes, bool bManualReset, bool bInitialState, string lpName);

		[DllImport("kernel32.dll", SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		static extern bool CloseHandle(IntPtr hObject);

		[DllImport("kernel32", SetLastError = true, ExactSpelling = true)]
		internal static extern Int32 WaitForSingleObject(IntPtr handle, int milliseconds);

		[DllImport("kernel32.dll")]
		static extern bool SetEvent(IntPtr hEvent);
		
		[DllImport("kernel32.dll")]
		static extern bool ResetEvent(IntPtr hEvent);

		[DllImport("kernel32.dll")]
		static extern bool PulseEvent(IntPtr hEvent);

		public event D2XXEventHandler Handler;

		public D2XXEvent(D2XX d2xx, int msk)
		{
			IntPtr evt = CreateEvent(IntPtr.Zero, false, false, "");
			d2xx.SetEventNotification(msk, evt);
			//! ... in loop
			WaitForSingleObject(evt, -1);
			// check if event was killed before calling handler
			int r, t, s;
			d2xx.GetStatus(out r, out t, out s);
			OnEvent(new D2XXEventArgs(r, t, s));
		}

		~D2XXEvent()
		{

		}

		protected virtual void OnEvent(D2XXEventArgs a)
		{
			if (Handler != null) Handler(this, a);
		}


	}


}

/*
typedef PVOID	FT_HANDLE;
typedef ULONG	FT_STATUS;

FT_STATUS WINAPI FT_Open(
	int deviceNumber,
	FT_HANDLE *pHandle
	);


FT_STATUS WINAPI FT_OpenEx(
    PVOID pArg1,
    DWORD Flags,
    FT_HANDLE *pHandle
    );

 
FT_STATUS WINAPI FT_ListDevices(
	PVOID pArg1,
	PVOID pArg2,
	DWORD Flags
	);


FT_STATUS WINAPI FT_Close(
    FT_HANDLE ftHandle
    );


FT_STATUS WINAPI FT_Read(
    FT_HANDLE ftHandle,
    LPVOID lpBuffer,
    DWORD nBufferSize,
    LPDWORD lpBytesReturned
    );

 
FT_STATUS WINAPI FT_Write(
    FT_HANDLE ftHandle,
    LPVOID lpBuffer,
    DWORD nBufferSize,
    LPDWORD lpBytesWritten
    );

 
FT_STATUS WINAPI FT_IoCtl(
    FT_HANDLE ftHandle,
    DWORD dwIoControlCode,
    LPVOID lpInBuf,
    DWORD nInBufSize,
    LPVOID lpOutBuf,
    DWORD nOutBufSize,
    LPDWORD lpBytesReturned,
    LPOVERLAPPED lpOverlapped
    );


FT_STATUS WINAPI FT_SetBaudRate(
    FT_HANDLE ftHandle,
	ULONG BaudRate
	);


FT_STATUS WINAPI FT_SetDivisor(
    FT_HANDLE ftHandle,
	USHORT Divisor
	);


FT_STATUS WINAPI FT_SetDataCharacteristics(
    FT_HANDLE ftHandle,
	UCHAR WordLength,
	UCHAR StopBits,
	UCHAR Parity
	);


FT_STATUS WINAPI FT_SetFlowControl(
    FT_HANDLE ftHandle,
    USHORT FlowControl,
    UCHAR XonChar,
    UCHAR XoffChar
	);


FT_STATUS WINAPI FT_ResetDevice(
    FT_HANDLE ftHandle
	);


FT_STATUS WINAPI FT_SetDtr(
    FT_HANDLE ftHandle
	);


FT_STATUS WINAPI FT_ClrDtr(
    FT_HANDLE ftHandle
	);


FT_STATUS WINAPI FT_SetRts(
    FT_HANDLE ftHandle
	);


FT_STATUS WINAPI FT_ClrRts(
    FT_HANDLE ftHandle
	);


FT_STATUS WINAPI FT_GetModemStatus(
    FT_HANDLE ftHandle,
	ULONG *pModemStatus
	);


FT_STATUS WINAPI FT_SetChars(
    FT_HANDLE ftHandle,
	UCHAR EventChar,
	UCHAR EventCharEnabled,
	UCHAR ErrorChar,
	UCHAR ErrorCharEnabled
    );


FT_STATUS WINAPI FT_Purge(
    FT_HANDLE ftHandle,
	ULONG Mask
	);


FT_STATUS WINAPI FT_SetTimeouts(
    FT_HANDLE ftHandle,
	ULONG ReadTimeout,
	ULONG WriteTimeout
	);


FT_STATUS WINAPI FT_GetQueueStatus(
    FT_HANDLE ftHandle,
	DWORD *dwRxBytes
	);


FT_STATUS WINAPI FT_SetEventNotification(
    FT_HANDLE ftHandle,
	DWORD Mask,
	PVOID Param
	);


FT_STATUS WINAPI FT_GetStatus(
    FT_HANDLE ftHandle,
    DWORD *dwRxBytes,
    DWORD *dwTxBytes,
    DWORD *dwEventDWord
	);


FT_STATUS WINAPI FT_SetBreakOn(
    FT_HANDLE ftHandle
    );


FT_STATUS WINAPI FT_SetBreakOff(
    FT_HANDLE ftHandle
    );


FT_STATUS WINAPI FT_SetWaitMask(
    FT_HANDLE ftHandle,
    DWORD Mask
    );


FT_STATUS WINAPI FT_WaitOnMask(
    FT_HANDLE ftHandle,
    DWORD *Mask
    );


FT_STATUS WINAPI FT_GetEventStatus(
    FT_HANDLE ftHandle,
    DWORD *dwEventDWord
    );


FT_STATUS WINAPI FT_ReadEE(
    FT_HANDLE ftHandle,
	DWORD dwWordOffset,
    LPWORD lpwValue
	);


FT_STATUS WINAPI FT_WriteEE(
    FT_HANDLE ftHandle,
	DWORD dwWordOffset,
    WORD wValue
	);


FT_STATUS WINAPI FT_EraseEE(
    FT_HANDLE ftHandle
	);

//
// structure to hold program data for FT_Program function
//
typedef struct ft_program_data {

	DWORD Signature1;			// Header - must be 0x00000000 
	DWORD Signature2;			// Header - must be 0xffffffff
	DWORD Version;				// Header - FT_PROGRAM_DATA version
								//          0 = original
	                            //          1 = FT2232C extensions
								//			2 = FT232R extensions

	WORD VendorId;				// 0x0403
	WORD ProductId;				// 0x6001
	char *Manufacturer;			// "FTDI"
	char *ManufacturerId;		// "FT"
	char *Description;			// "USB HS Serial Converter"
	char *SerialNumber;			// "FT000001" if fixed, or NULL
	WORD MaxPower;				// 0 < MaxPower <= 500
	WORD PnP;					// 0 = disabled, 1 = enabled
	WORD SelfPowered;			// 0 = bus powered, 1 = self powered
	WORD RemoteWakeup;			// 0 = not capable, 1 = capable
	//
	// Rev4 extensions
	//
	UCHAR Rev4;					// non-zero if Rev4 chip, zero otherwise
	UCHAR IsoIn;				// non-zero if in endpoint is isochronous
	UCHAR IsoOut;				// non-zero if out endpoint is isochronous
	UCHAR PullDownEnable;		// non-zero if pull down enabled
	UCHAR SerNumEnable;			// non-zero if serial number to be used
	UCHAR USBVersionEnable;		// non-zero if chip uses USBVersion
	WORD USBVersion;			// BCD (0x0200 => USB2)
	//
	// FT2232C extensions
	//
	UCHAR Rev5;					// non-zero if Rev5 chip, zero otherwise
	UCHAR IsoInA;				// non-zero if in endpoint is isochronous
	UCHAR IsoInB;				// non-zero if in endpoint is isochronous
	UCHAR IsoOutA;				// non-zero if out endpoint is isochronous
	UCHAR IsoOutB;				// non-zero if out endpoint is isochronous
	UCHAR PullDownEnable5;		// non-zero if pull down enabled
	UCHAR SerNumEnable5;		// non-zero if serial number to be used
	UCHAR USBVersionEnable5;	// non-zero if chip uses USBVersion
	WORD USBVersion5;			// BCD (0x0200 => USB2)
	UCHAR AIsHighCurrent;		// non-zero if interface is high current
	UCHAR BIsHighCurrent;		// non-zero if interface is high current
	UCHAR IFAIsFifo;			// non-zero if interface is 245 FIFO
	UCHAR IFAIsFifoTar;			// non-zero if interface is 245 FIFO CPU target
	UCHAR IFAIsFastSer;			// non-zero if interface is Fast serial
	UCHAR AIsVCP;				// non-zero if interface is to use VCP drivers
	UCHAR IFBIsFifo;			// non-zero if interface is 245 FIFO
	UCHAR IFBIsFifoTar;			// non-zero if interface is 245 FIFO CPU target
	UCHAR IFBIsFastSer;			// non-zero if interface is Fast serial
	UCHAR BIsVCP;				// non-zero if interface is to use VCP drivers
	//
	// FT232R extensions
	//
	UCHAR UseExtOsc;			// Use External Oscillator
	UCHAR HighDriveIOs;			// High Drive I/Os
	UCHAR EndpointSize;			// Endpoint size

	UCHAR PullDownEnableR;		// non-zero if pull down enabled
	UCHAR SerNumEnableR;		// non-zero if serial number to be used

	UCHAR InvertTXD;			// non-zero if invert TXD
	UCHAR InvertRXD;			// non-zero if invert RXD
	UCHAR InvertRTS;			// non-zero if invert RTS
	UCHAR InvertCTS;			// non-zero if invert CTS
	UCHAR InvertDTR;			// non-zero if invert DTR
	UCHAR InvertDSR;			// non-zero if invert DSR
	UCHAR InvertDCD;			// non-zero if invert DCD
	UCHAR InvertRI;				// non-zero if invert RI

	UCHAR Cbus0;				// Cbus Mux control
	UCHAR Cbus1;				// Cbus Mux control
	UCHAR Cbus2;				// Cbus Mux control
	UCHAR Cbus3;				// Cbus Mux control
	UCHAR Cbus4;				// Cbus Mux control

	UCHAR RIsD2XX;				// non-zero if using D2XX driver

} FT_PROGRAM_DATA, *PFT_PROGRAM_DATA;


FT_STATUS WINAPI FT_EE_Program(
    FT_HANDLE ftHandle,
	PFT_PROGRAM_DATA pData
	);


FT_STATUS WINAPI FT_EE_ProgramEx(
    FT_HANDLE ftHandle,
	PFT_PROGRAM_DATA pData,
	char *Manufacturer,
	char *ManufacturerId,
	char *Description,
	char *SerialNumber
	);


FT_STATUS WINAPI FT_EE_Read(
    FT_HANDLE ftHandle,
	PFT_PROGRAM_DATA pData
	);


FT_STATUS WINAPI FT_EE_ReadEx(
    FT_HANDLE ftHandle,
	PFT_PROGRAM_DATA pData,
	char *Manufacturer,
	char *ManufacturerId,
	char *Description,
	char *SerialNumber
	);


FT_STATUS WINAPI FT_EE_UASize(
    FT_HANDLE ftHandle,
	LPDWORD lpdwSize
	);


FT_STATUS WINAPI FT_EE_UAWrite(
    FT_HANDLE ftHandle,
	PUCHAR pucData,
	DWORD dwDataLen
	);


FT_STATUS WINAPI FT_EE_UARead(
    FT_HANDLE ftHandle,
	PUCHAR pucData,
	DWORD dwDataLen,
	LPDWORD lpdwBytesRead
	);


FT_STATUS WINAPI FT_SetLatencyTimer(
    FT_HANDLE ftHandle,
    UCHAR ucLatency
    );


FT_STATUS WINAPI FT_GetLatencyTimer(
    FT_HANDLE ftHandle,
    PUCHAR pucLatency
    );


FT_STATUS WINAPI FT_SetBitMode(
    FT_HANDLE ftHandle,
    UCHAR ucMask,
	UCHAR ucEnable
    );


FT_STATUS WINAPI FT_GetBitMode(
    FT_HANDLE ftHandle,
    PUCHAR pucMode
    );


FT_STATUS WINAPI FT_SetUSBParameters(
    FT_HANDLE ftHandle,
    ULONG ulInTransferSize,
    ULONG ulOutTransferSize
	);


FT_STATUS WINAPI FT_SetDeadmanTimeout(
    FT_HANDLE ftHandle,
	ULONG ulDeadmanTimeout
    );


FT_STATUS WINAPI FT_GetDeviceInfo(
    FT_HANDLE ftHandle,
    FT_DEVICE *lpftDevice,
	LPDWORD lpdwID,
	PCHAR SerialNumber,
	PCHAR Description,
	LPVOID Dummy
    );


FT_STATUS WINAPI FT_StopInTask(
    FT_HANDLE ftHandle
    );


FT_STATUS WINAPI FT_RestartInTask(
    FT_HANDLE ftHandle
    );


FT_STATUS WINAPI FT_SetResetPipeRetryCount(
    FT_HANDLE ftHandle,
	DWORD dwCount
    );


FT_STATUS WINAPI FT_ResetPort(
    FT_HANDLE ftHandle
    );


FT_STATUS WINAPI FT_CyclePort(
    FT_HANDLE ftHandle
    );

//
// Device information
//

typedef struct _ft_device_list_info_node {
	ULONG Flags;
    ULONG Type;
	ULONG ID;
	DWORD LocId;
	char SerialNumber[16];
	char Description[64];
	FT_HANDLE ftHandle;
} FT_DEVICE_LIST_INFO_NODE;



FT_STATUS WINAPI FT_CreateDeviceInfoList(
	LPDWORD lpdwNumDevs
	);


FT_STATUS WINAPI FT_GetDeviceInfoList(
	FT_DEVICE_LIST_INFO_NODE *pDest,
	LPDWORD lpdwNumDevs
	);


FT_STATUS WINAPI FT_GetDeviceInfoDetail(
	DWORD dwIndex,
	LPDWORD lpdwFlags,
	LPDWORD lpdwType,
	LPDWORD lpdwID,
	LPDWORD lpdwLocId,
	LPVOID lpSerialNumber,
	LPVOID lpDescription,
	FT_HANDLE *pftHandle
	);


//
// Version information
//


FT_STATUS WINAPI FT_GetDriverVersion(
    FT_HANDLE ftHandle,
	LPDWORD lpdwVersion
	);


FT_STATUS WINAPI FT_GetLibraryVersion(
	LPDWORD lpdwVersion
	);



FT_STATUS WINAPI FT_Rescan(
	void
	);


FT_STATUS WINAPI FT_Reload(
	WORD wVid,
	WORD wPid
	);

*/