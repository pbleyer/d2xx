#ifndef PY_D2XX_H_
#define PY_D2XX_H_

/*
	Copyright (C) 2007 Pablo Bleyer Kocik

	This library is free software; you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public
	License as published by the Free Software Foundation; either
	version 2.1 of the License, or (at your option) any later version.

	This library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/


#include <Python.h>

#ifdef STDC_HEADERS
#include <stddef.h>
#else
	#ifndef DONT_HAVE_SYS_TYPES_H
	#include <sys/types.h>
	#endif
#endif

#ifdef WIN32
#include <windows.h>
#else
#include <unistd.h>
#include <fcntl.h>
#include <sys/io.h>
// #include <linux/ioctl.h>
#include "WinTypes.h"
#endif

#ifdef __GNUC__
#undef WINAPI
#define WINAPI
#endif
#include <ftd2xx.h>

// enable WIN32 API functions
// #define WIN32_API

/* Types */
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned long u32;
typedef signed char i8;
typedef signed short i16;
typedef signed long i32;

/* Defines */
#define DESCRIPTION_SIZE 256 ///< For serial numbers and descriptions
#define MAX_DEVICES 64

static PyObject *Fterr; ///< Error object

typedef struct _Ftstat _Ftstat; ///< FT_STATUS values and their descriptions
struct _Ftstat {
	FT_STATUS status;
	const char* description;
};

static const _Ftstat Ftstat[] = {
	{FT_OK, "Ok"},
	{FT_INVALID_HANDLE, "Invalid handle."},
	{FT_DEVICE_NOT_FOUND, "Device not found."},
	{FT_DEVICE_NOT_OPENED, "Not opened."},
	{FT_IO_ERROR, "IO error"},
	{FT_INSUFFICIENT_RESOURCES, "Insufficient resources."},
	{FT_INVALID_PARAMETER, "Invalid parameter."},
	{FT_INVALID_BAUD_RATE, "Invalid baud rate."},
	{FT_DEVICE_NOT_OPENED_FOR_ERASE, "Device not opened for erase."},
	{FT_DEVICE_NOT_OPENED_FOR_WRITE, "Device not opened for write."},
	{FT_FAILED_TO_WRITE_DEVICE, "Failed to write device."},
	{FT_EEPROM_READ_FAILED, "EEPROM read failed."},
	{FT_EEPROM_WRITE_FAILED, "EEPROM write failed."},
	{FT_EEPROM_ERASE_FAILED, "EEPROM erase failed."},
	{FT_EEPROM_NOT_PRESENT, "EEPROM not present."},
	{FT_EEPROM_NOT_PROGRAMMED, "EEPROM not programmed."},
	{FT_INVALID_ARGS, "Invalid args."},
	{FT_NOT_SUPPORTED, "Not supported."},
	{FT_OTHER_ERROR, "Other error."},
	{FT_DEVICE_LIST_NOT_READY, "Device list not ready."}
};

// enum {};

#define Ftobj_Check(v)	((v)->ob_type == &Ftobj_Type) ///< Check for object type
#define Ftobj_OPEN (1<<0) ///< Object is opened (in status field)

typedef struct {
	PyObject_HEAD

	unsigned status; // device oject status
	FT_HANDLE handle; // device handle
#ifdef WIN32_API
	OVERLAPPED overlap;
#endif
} Ftobj;

static PyTypeObject Ftobj_Type;

/* Declarations */
static PyObject* ftobj_alloc(FT_HANDLE);
static void ftobj_dealloc(Ftobj *);
static PyObject* ftobj_getattr(Ftobj *fo, char *name);

static PyObject* ftobj_GetLibraryVersion(Ftobj *fo, PyObject *ao);
// LPDWORD lpdwDLLVersion

static PyObject* ftobj_Rescan(Ftobj *fo, PyObject *ao);
// void

static PyObject* ftobj_Reload(Ftobj *fo, PyObject *ao);
//	WORD wVid,
//	WORD wPid

static PyObject* ftobj_CreateDeviceInfoList(Ftobj *fo, PyObject *ao);
// LPDWORD lpdwNumDevs

static PyObject* ftobj_Open(Ftobj *fo, PyObject *ao);
// int deviceNumber,
// FT_HANDLE *pHandle

static PyObject* ftobj_OpenEx(Ftobj *fo, PyObject *ao);
// PVOID pArg1,
// DWORD Flags,
// FT_HANDLE *pHandle

static PyObject* ftobj_ListDevices(Ftobj *fo, PyObject *ao);
// PVOID pArg1,
// PVOID pArg2,
// DWORD Flags

#ifndef WIN32
static PyObject* ftobj_SetVIDPID(Ftobj *fo, PyObject *ao);
// DWORD dwVID,
// DWORD dwPID

static PyObject* ftobj_GetVIDPID(Ftobj *fo, PyObject *ao);
// DWORD * pdwVID,
// DWORD * pdwPID
#endif

static PyObject* ftobj_Close(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle

static PyObject* ftobj_Read(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
// LPVOID lpBuffer,
// DWORD nBufferSize,
// LPDWORD lpBytesReturned

static PyObject* ftobj_Write(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
// LPVOID lpBuffer,
// DWORD nBufferSize,
// LPDWORD lpBytesWritten

static PyObject* ftobj_IoCtl(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
// DWORD dwIoControlCode,
// LPVOID lpInBuf,
// DWORD nInBufSize,
// LPVOID lpOutBuf,
// DWORD nOutBufSize,
// LPDWORD lpBytesReturned,
// LPOVERLAPPED lpOverlapped

static PyObject* ftobj_SetBaudRate(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
// ULONG BaudRate

static PyObject* ftobj_SetDivisor(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
// USHORT Divisor

static PyObject* ftobj_SetDataCharacteristics(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
// UCHAR WordLength,
// UCHAR StopBits,
// UCHAR Parity

static PyObject* ftobj_SetFlowControl(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
// USHORT FlowControl,
// UCHAR XonChar,
// UCHAR XoffChar

static PyObject* ftobj_ResetDevice(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle

static PyObject* ftobj_SetDtr(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle

static PyObject* ftobj_ClrDtr(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle

static PyObject* ftobj_SetRts(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle

static PyObject* ftobj_ClrRts(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle

static PyObject* ftobj_GetModemStatus(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
//	ULONG *pModemStatus

static PyObject* ftobj_SetChars(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
// UCHAR EventChar,
// UCHAR EventCharEnabled,
// UCHAR ErrorChar,
// UCHAR ErrorCharEnabled

static PyObject* ftobj_Purge(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
// ULONG Mask

static PyObject* ftobj_SetTimeouts(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
// ULONG ReadTimeout,
// ULONG WriteTimeout

static PyObject* ftobj_SetDeadmanTimeout(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
// ULONG ulDeadmanTimeout,

static PyObject* ftobj_GetQueueStatus(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
// DWORD *dwRxBytes

static PyObject* ftobj_SetEventNotification(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
// DWORD Mask,
// PVOID Param

static PyObject* ftobj_GetStatus(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
// DWORD *dwRxBytes,
// DWORD *dwTxBytes,
// DWORD *dwEventDWord

static PyObject* ftobj_SetBreakOn(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle

static PyObject* ftobj_SetBreakOff(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle

static PyObject* ftobj_SetWaitMask(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
// DWORD Mask

static PyObject* ftobj_WaitOnMask(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
// DWORD *Mask

static PyObject* ftobj_GetEventStatus(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
// DWORD *dwEventDWord

//static PyObject* ftobj_ReadEE(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
// DWORD dwWordOffset,
// LPWORD lpwValue
//
//static PyObject* ftobj_WriteEE(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
// DWORD dwWordOffset,
// WORD wValue
//
//static PyObject* ftobj_EraseEE(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle

// structure to hold program data for FT_Program function
//
//typedef struct ft_program_data {
//	WORD VendorId;				// 0x0403
//	WORD ProductId;				// 0x6001
//	char *Manufacturer;			// "FTDI"
//	char *ManufacturerId;		// "FT"
//	char *Description;			// "USB HS Serial Converter"
//	char *SerialNumber;			// "FT000001" if fixed, or NULL
//	WORD MaxPower;				// 0 < MaxPower <= 500
//	WORD PnP;					// 0 = disabled, 1 = enabled
//	WORD SelfPowered;			// 0 = bus powered, 1 = self powered
//	WORD RemoteWakeup;			// 0 = not capable, 1 = capable
//	//
//	// Rev4 extensions
//	//
//	UCHAR Rev4;					// non-zero if Rev4 chip, zero otherwise
//	UCHAR IsoIn;				// non-zero if in endpoint is isochronous
//	UCHAR IsoOut;				// non-zero if out endpoint is isochronous
//	UCHAR PullDownEnable;		// non-zero if pull down enabled
//	UCHAR SerNumEnable;			// non-zero if serial number to be used
//	UCHAR USBVersionEnable;		// non-zero if chip uses USBVersion
//	WORD USBVersion;			// BCD (0x0200 => USB2)
//} FT_PROGRAM_DATA, *PFT_PROGRAM_DATA;

static PyObject* ftobj_EE_Program(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
// PFT_PROGRAM_DATA pData

static PyObject* ftobj_EE_Read(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
// PFT_PROGRAM_DATA pData

static PyObject* ftobj_EE_UASize(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
// LPDWORD lpdwSize

static PyObject* ftobj_EE_UAWrite(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
// PUCHAR pucData,
// DWORD dwDataLen

static PyObject* ftobj_EE_UARead(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
// PUCHAR pucData,
// DWORD dwDataLen,
// LPDWORD lpdwBytesRead

static PyObject* ftobj_SetLatencyTimer(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
// UCHAR ucLatency

static PyObject* ftobj_GetLatencyTimer(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
// PUCHAR pucLatency

static PyObject* ftobj_SetBitMode(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
// UCHAR ucMask,
// UCHAR ucEnable

static PyObject* ftobj_GetBitMode(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
// PUCHAR pucMode

static PyObject* ftobj_SetUSBParameters(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
// ULONG ulInTransferSize,
// ULONG ulOutTransferSize

static PyObject* ftobj_GetDeviceInfo(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
// FT_DEVICE *lpftDevice,
// LPDWORD lpdwID,
// PCHAR SerialNumber,
// PCHAR Description,
// LPVOID Dummy

static PyObject* ftobj_StopInTask(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle

static PyObject* ftobj_RestartInTask(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle

static PyObject* ftobj_SetResetPipeRetryCount(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
// DWORD dwCount

static PyObject* ftobj_ResetPort(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle

static PyObject* ftobj_CyclePort(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle

static PyObject* ftobj_GetDriverVersion(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
// LPDWORD lpdwDriverVersion

static PyObject* ftobj_GetComPortNumber(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
// LPLONG lpdwComPortNumber

static PyObject* ftobj_EE_ReadConfig(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
// UCHAR ucAddress,
// PUCHAR pucValue

static PyObject* ftobj_EE_WriteConfig(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
// UCHAR ucAddress,
// UCHAR ucValue

static PyObject* ftobj_EE_ReadEcc(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
// UCHAR ucOption,
// LPWORD lpwValue

static PyObject* ftobj_GetQueueStatusEx(Ftobj *fo, PyObject *ao);
// FT_HANDLE ftHandle,
// DWORD *dwRxBytes

//
//
// Win32-type functions
//

#ifdef WIN32_API

static PyObject* ftobj_W32_CreateFile(Ftobj *fo, PyObject *ao);
//FT_HANDLE FT_W32_CreateFile(
//	PVOID					pvArg1,
//	DWORD					dwAccess,
//	DWORD					dwShareMode,
//	LPSECURITY_ATTRIBUTES	lpSecurityAttributes,
//	DWORD					dwCreate,
//	DWORD					dwAttrsAndFlags,
//	HANDLE					hTemplate

#if 0

static PyObject* ftobj_W32_CloseHandle(Ftobj *fo, PyObject *ao);
//BOOL FT_W32_CloseHandle(
// FT_HANDLE ftHandle

static PyObject* ftobj_W32_ReadFile(Ftobj *fo, PyObject *ao);
//BOOL FT_W32_ReadFile(
// FT_HANDLE ftHandle,
//    LPVOID lpBuffer,
//    DWORD nBufferSize,
//    LPDWORD lpBytesReturned,
//	LPOVERLAPPED lpOverlapped

static PyObject* ftobj_W32_WriteFile(Ftobj *fo, PyObject *ao);
//BOOL FT_W32_WriteFile(
// FT_HANDLE ftHandle,
//    LPVOID lpBuffer,
//    DWORD nBufferSize,
//    LPDWORD lpBytesWritten,
//	LPOVERLAPPED lpOverlapped

static PyObject* ftobj_W32_GetLastError(Ftobj *fo, PyObject *ao);
//DWORD FT_W32_GetLastError(
// FT_HANDLE ftHandle

static PyObject* ftobj_W32_GetOverlappedResult(Ftobj *fo, PyObject *ao);
//BOOL FT_W32_GetOverlappedResult(
// FT_HANDLE ftHandle,
//	LPOVERLAPPED lpOverlapped,
//    LPDWORD lpdwBytesTransferred,
//	BOOL bWait

static PyObject* ftobj_W32_CancelIo(Ftobj *fo, PyObject *ao);
//BOOL FT_W32_CancelIo(
// FT_HANDLE ftHandle

////
//// Win32 COMM API type functions
////
//typedef struct _FTCOMSTAT {
//    DWORD fCtsHold : 1;
//    DWORD fDsrHold : 1;
//    DWORD fRlsdHold : 1;
//    DWORD fXoffHold : 1;
//    DWORD fXoffSent : 1;
//    DWORD fEof : 1;
//    DWORD fTxim : 1;
//    DWORD fReserved : 25;
//    DWORD cbInQue;
//    DWORD cbOutQue;
//} FTCOMSTAT, *LPFTCOMSTAT;

//typedef struct _FTDCB {
//    DWORD DCBlength;      /* sizeof(FTDCB)                   */
//    DWORD BaudRate;       /* Baudrate at which running       */
//    DWORD fBinary: 1;     /* Binary Mode (skip EOF check)    */
//    DWORD fParity: 1;     /* Enable parity checking          */
//    DWORD fOutxCtsFlow:1; /* CTS handshaking on output       */
//    DWORD fOutxDsrFlow:1; /* DSR handshaking on output       */
//    DWORD fDtrControl:2;  /* DTR Flow control                */
//    DWORD fDsrSensitivity:1; /* DSR Sensitivity              */
//    DWORD fTXContinueOnXoff: 1; /* Continue TX when Xoff sent */
//    DWORD fOutX: 1;       /* Enable output X-ON/X-OFF        */
//    DWORD fInX: 1;        /* Enable input X-ON/X-OFF         */
//    DWORD fErrorChar: 1;  /* Enable Err Replacement          */
//    DWORD fNull: 1;       /* Enable Null stripping           */
//    DWORD fRtsControl:2;  /* Rts Flow control                */
//    DWORD fAbortOnError:1; /* Abort all reads and writes on Error */
//    DWORD fDummy2:17;     /* Reserved                        */
//    WORD wReserved;       /* Not currently used              */
//    WORD XonLim;          /* Transmit X-ON threshold         */
//    WORD XoffLim;         /* Transmit X-OFF threshold        */
//    BYTE ByteSize;        /* Number of bits/byte, 4-8        */
//    BYTE Parity;          /* 0-4=None,Odd,Even,Mark,Space    */
//    BYTE StopBits;        /* 0,1,2 = 1, 1.5, 2               */
//    char XonChar;         /* Tx and Rx X-ON character        */
//    char XoffChar;        /* Tx and Rx X-OFF character       */
//    char ErrorChar;       /* Error replacement char          */
//    char EofChar;         /* End of Input character          */
//    char EvtChar;         /* Received Event character        */
//    WORD wReserved1;      /* Fill for now.                   */
//} FTDCB, *LPFTDCB;
//
//typedef struct _FTTIMEOUTS {
//    DWORD ReadIntervalTimeout;          /* Maximum time between read chars. */
//    DWORD ReadTotalTimeoutMultiplier;   /* Multiplier of characters.        */
//    DWORD ReadTotalTimeoutConstant;     /* Constant in milliseconds.        */
//    DWORD WriteTotalTimeoutMultiplier;  /* Multiplier of characters.        */
//    DWORD WriteTotalTimeoutConstant;    /* Constant in milliseconds.        */
//} FTTIMEOUTS,*LPFTTIMEOUTS;

static PyObject* ftobj_W32_ClearCommBreak(Ftobj *fo, PyObject *ao);
//BOOL FT_W32_ClearCommBreak(
// FT_HANDLE ftHandle

static PyObject* ftobj_W32_ClearCommError(Ftobj *fo, PyObject *ao);
//BOOL FT_W32_ClearCommError(
// FT_HANDLE ftHandle,
//	LPDWORD lpdwErrors,
//    LPFTCOMSTAT lpftComstat

static PyObject* ftobj_W32_EscapeCommFunction(Ftobj *fo, PyObject *ao);
//BOOL FT_W32_EscapeCommFunction(
// FT_HANDLE ftHandle,
//	DWORD dwFunc

static PyObject* ftobj_W32_GetCommModemStatus(Ftobj *fo, PyObject *ao);
//BOOL FT_W32_GetCommModemStatus(
// FT_HANDLE ftHandle,
//	LPDWORD lpdwModemStatus

static PyObject* ftobj_W32_GetCommState(Ftobj *fo, PyObject *ao);
//BOOL FT_W32_GetCommState(
// FT_HANDLE ftHandle,
//    LPFTDCB lpftDcb

static PyObject* ftobj_W32_GetCommTimeouts(Ftobj *fo, PyObject *ao);
//BOOL FT_W32_GetCommTimeouts(
// FT_HANDLE ftHandle,
// FTTIMEOUTS *pTimeouts

static PyObject* ftobj_W32_PurgeComm(Ftobj *fo, PyObject *ao);
//BOOL FT_W32_PurgeComm(
// FT_HANDLE ftHandle,
//	DWORD dwMask

static PyObject* ftobj_W32_SetCommBreak(Ftobj *fo, PyObject *ao);
//BOOL FT_W32_SetCommBreak(
// FT_HANDLE ftHandle

static PyObject* ftobj_W32_SetCommMask(Ftobj *fo, PyObject *ao);
//BOOL FT_W32_SetCommMask(
// FT_HANDLE ftHandle,
//    ULONG ulEventMask

static PyObject* ftobj_W32_SetCommState(Ftobj *fo, PyObject *ao);
//BOOL FT_W32_SetCommState(
// FT_HANDLE ftHandle,
//    LPFTDCB lpftDcb

static PyObject* ftobj_W32_SetCommTimeouts(Ftobj *fo, PyObject *ao);
//BOOL FT_W32_SetCommTimeouts(
// FT_HANDLE ftHandle,
// FTTIMEOUTS *pTimeouts

static PyObject* ftobj_W32_SetupComm(Ftobj *fo, PyObject *ao);
//BOOL FT_W32_SetupComm(
// FT_HANDLE ftHandle,
//	DWORD dwReadBufferSize,
//	DWORD dwWriteBufferSize

static PyObject* ftobj_W32_WaitCommEvent(Ftobj *fo, PyObject *ao);
//BOOL FT_W32_WaitCommEvent(
// FT_HANDLE ftHandle,
//    PULONG pulEvent,
//	LPOVERLAPPED lpOverlapped

#endif // 0

#endif // WIN32_API

#endif // PY_D2XX_H_

