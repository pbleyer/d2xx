# JD2XX

Java JNI interface for the FTDI D2XX driver.

## Building

The provided Makefiles will build a JAR distribution and JNI shared object or
dynamic library. The target 32 or 64bit architecure can be selected using the
FTDIARCH define.

FTDIARCH={32,64} make

### Example
* Use GCC to build a 32-bit version of the library that will reside in the
lib32 subdirectory and specifying the location of the JDK:

	CC=gcc FTDIARCH=32 JDK=/lib/jvm/java-14-openjdk-amd64 make

* Use Clang to build a 64-bit version of the library that will reside in the
lib64 subdirectory:

	CC=clang FTDIARCH=64 make

## Installing

Use the generated jd2xx.jar and libJD2XX.so (POSIX) or JD2XX.dll (Windows)
library in your application.
